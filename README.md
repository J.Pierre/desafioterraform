# Desafio Terraform Five
## Criar 3 ambientes usando terraform na AWS:
* 1º com 3 maquinas + 1 banco de dados postgres com uma maquina dependente do postgres.
* 2º com 2 maquinas + 1 banco de dados mysql com uma maquina depente do mysql.
* 3º ambiente com uma maquina com Docker que rode uma palicação em Java!

### Pré requisitos:
- Necessario criar um usuario na "AWS-IAM" para que possa ser provisonado as maquinas
- Configurar chave de acesso ssh entre o terraform e aws (coloque  a chave publica na raiz do ambiente)
- Baixar e configurar AWS CLI com usuario criado na aws-iam


#### Ambiente 1
- com as configuraçoes feitas acesse o terminal apartir da raiz do ambiente e de o comando

~~~HTML
terraform plan
terraform apply
~~~

#### Ambiente 2
- com as configuraçoes feitas acesse o terminal apartir da raiz do ambiente e de o comando

~~~HTML
terraform plan
terraform apply
~~~

#### Ambiente 3
1. com as configuraçoes feitas acesse o terminal apartir da raiz do ambiente e de o comando

~~~HTML
terraform plan
terraform apply
~~~

2. configurando ansible para rodar o Docker com hello-world java:

- Depois que maquina estiver de pé, vá até aws > ec2 > intâncias em exeção clique em conectar 
e copie a configuração de cexão ssh a partir do "@" cole no aquivo de hosts do ansible sem apagar a parte de usurio 

- depois do ansible_ssh_private_key_file="" especifique o caminho da chave privada ssh configurada na aws para o ansible poder se conectar

- no arquivo provisioning.yml na tesks de copiar os arquivos, troque o diretorio dos arquivos para o diretorio do ambiente no seu computador

- abra o terminal apartir da pasta do ansble o rede o comando : " ansible-playbook -i hosts provisioning.yml ".

