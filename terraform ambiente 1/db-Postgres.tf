#Criando Postgres e vinculando com o vpc
resource "aws_db_instance" "postgres" {
  allocated_storage    = 10
  engine               = "postgres"
  engine_version       = "13.4"
  instance_class       = "db.t3.micro"
  name                 = "mydb"
  username             = "five"
  password             = "five12345"
  skip_final_snapshot  = true

  vpc_security_group_ids = ["${aws_security_group.conexao-ssh.id}","sg-0a41949bf49b8da38"]
}