#Criando um vpc para acesso remoto
resource "aws_security_group" "conexao-ssh" {
  name        = "conexao-ssh"
  description = "conexao-ssh"

  ingress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "ssh"
  }
}