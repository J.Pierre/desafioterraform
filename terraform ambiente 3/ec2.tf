#Criando 1 instâcia EC2
resource "aws_instance" "ambiente2" {
  ami = "ami-052efd3df9dad4825"
  instance_type =  "t2.micro"
  key_name =  "terraform_aws"
  tags = {
    "Name" = "ambiente3"
  }

  vpc_security_group_ids = ["${aws_security_group.conexao-ssh.id}","sg-0a41949bf49b8da38"]
}

