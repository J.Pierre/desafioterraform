#Criando Postgres
resource "aws_db_instance" "mysql" {
  allocated_storage    = 10
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t3.micro"
  name                 = "mydb"
  username             = "five"
  password             = "five12345"
  skip_final_snapshot  = true

  vpc_security_group_ids = ["${aws_security_group.conexao-ssh.id}","sg-0a41949bf49b8da38"]
}