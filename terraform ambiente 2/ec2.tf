#Criando 2 instâcia EC2
resource "aws_instance" "ambiente2" {
  ami = "ami-052efd3df9dad4825"
  instance_type =  "t2.micro"
  key_name =  "terraform_aws"
  tags = {
    "Name" = "ambiente2"
  }

  vpc_security_group_ids = ["${aws_security_group.conexao-ssh.id}","sg-0a41949bf49b8da38"]
}

#Criando uma instância EC2 com Dependencia do Postgres
resource "aws_instance" "dependencia" {
  ami = "ami-052efd3df9dad4825"
  instance_type =  "t2.micro"
  key_name =  "terraform_aws"
  tags = {
    "Name" = "dependente"
  }
  vpc_security_group_ids = ["${aws_security_group.conexao-ssh.id}","sg-0a41949bf49b8da38"]

  #Declarando a dependencia
  depends_on = [aws_db_instance.mysql]
}
